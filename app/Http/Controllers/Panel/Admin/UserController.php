<?php

namespace App\Http\Controllers\Panel\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PanelUserRequest;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\Contracts\RoleRepositoryInterface;

/**
 * UserController - Painel Admin
 *
 * @author João Gustavo Balestrin dos Santos <joaogustavo.b@hotmail.com>
 * @copyright (c) 2016
 * @package LaravelPanel
 */
class UserController extends Controller
{
    /**
     * Instancia do UserRepository
     *
     * @var UserRepositoryInterface
     */
    protected $repository;

    /**
     * Instancia do RoleRepository
     *
     * @var UserRepositoryInterface
     */
    protected $roleRepository;

    /**
     * UserController construtor.
     *
     * @param UserRepositoryInterface $repository
     * @param RoleRepositoryInterface $roleRepository
     */
    function __construct(UserRepositoryInterface $repository, RoleRepositoryInterface $roleRepository)
    {
        $this->repository = $repository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Lista os usuários
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Busca os usuários mais recentes
        $users = $this->repository->getAllRecent(null);

        // Retorna a view de listar
        return view('panel.admin.user.index', compact('users'));
    }

    /**
     * Retorna a view de cadastrar
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Busca todos os grupos
        $roles = $this->roleRepository->getAll();

        // Retorna a view de cadastrar
        return view('panel.admin.user.create', compact('roles'));
    }

    /**
     * Insere o usuário na base de dados
     *
     * @param PanelUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PanelUserRequest $request)
    {
        // Realiza a inserção na base de dados
        $create = $this->repository->create($request->all());

        // Verifica se a ação foi efetuada com sucesso
        if (!$create) {
            // Retorna uma mensagem de erro
            return response()->json(['type' => 'error', 'msg' => 'Falha no cadastro do usuário', 'redirect' => route('panel.admin.users.index')]);
        }

        // Retorna uma mensagem de sucesso
        return response()->json(['type' => 'success', 'msg' => 'Cadastrado com Sucesso!', 'redirect' => route('panel.admin.users.index')]);
    }

    /**
     * Exibi o usuário encontrado
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        // Busca o usuário respectivo pelo ID
        $user = $this->repository->findByID($id);

        // Retorna a view de visualizar
        return view('panel.admin.user.show', compact('user'));
    }

    /**
     * Exibi o formulário de editar
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        // Busca o usuário respectivo pelo ID
        $user = $this->repository->findByID($id);

        // Busca todos os grupos
        $roles = $this->roleRepository->getAll();

        // Retorna a view de editar
        return view('panel.admin.user.edit', compact('user', 'roles'));
    }

    /**
     * Atualiza o usuário na base de dados
     *
     * @param PanelUserRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PanelUserRequest $request, $id)
    {
        // Busca o usuário respectivo pelo ID
        $user = $this->repository->findByID($id);

        // Captura todos os dados
        $data = $request->all();

        // Verifica se o grupo do usuário é admin e se usuário é diferente do logado
        if ($user->role->name == 'admin' and $user->id != auth()->user()->id) {
            unset($data['password']);
        }

        // Realizar o update no repository
        $update = $this->repository->update($user, $data);

        // Verifica se houve alguma falha no update
        if (!$update) {
            // Retorna uma mensagem de erro
            return response()->json(['type' => 'error', 'msg' => 'Falha na edição do usuário', 'redirect' => route('panel.admin.users.index')]);
        }

        // Retorna uma mensagem de sucesso
        return response()->json(['type' => 'success', 'msg'  => 'Editado com Sucesso!', 'redirect' => route('panel.admin.users.index')]);
    }
}