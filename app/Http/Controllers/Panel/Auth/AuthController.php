<?php

namespace App\Http\Controllers\Panel\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Cache\RateLimiter;

/**
 * AuthController - Painel
 *
 * @author João Gustavo Balestrin dos Santos <joaogustavo.b@hotmail.com>
 * @copyright (c) 2016
 * @package LaravelPanel
 */
class AuthController extends Controller
{
    use ThrottlesLogins;

    /**
     * Tempo em segundos para bloquear login
     *
     * @var int
     */
    protected $lockoutTime = 720;

    /**
     * Tentativas máximas para bloquear login
     *
     * @var int
     */
    protected $maxLoginAttempts = 8;

    /**
     * Local aonde vai ser redirecionado após o login
     *
     * @var string
     */
    protected $redirectPath = '/';

    /**
     * Local aonde vai ser redirecionado após falha no Login
     *
     * @var string
     */
    protected $loginPath = '/login';

    /**
     * Local aonde vai se redirecionado após o logout
     *
     * @var string
     */
    protected $redirectAfterLogout = '/login';

    /**
     * AuthController construtor.
     */
    public function __construct()
    {
        // Middleware Guest
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Retorna a view de login
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin()
    {
        // Retorna a view de Login
        return view('panel.auth.login');
    }

    /**
     * Realiza o login
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postLogin(Request $request)
    {
        // Valida os dados
        $this->validate($request, [
            $this->loginUsername() => 'required|email', 'password' => 'required',
        ]);

        // Se a classe está usando o traço ThrottlesLogins , podemos estrangular automaticamente
        // o login tenta para esta aplicação. Vamos introduzir isso o nome de usuário e
        // o endereço IP do cliente fazendo essas solicitações para este aplicativo.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = array_add($this->getCredentials($request), 'status', 1);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // Se a tentativa de login não foi bem sucedida , vamos incrementar o número de tentativas
        // para acessar e redirecionar o usuário de volta para o formulário de login. Claro que, quando este
        // usuário ultrapassa o número máximo de tentativas de eles vão ficar trancado para fora.

        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        // Retorna mensagem de erro "e-mail ou senha inválidos"
        $msg['type'] = 'error';
        $msg['msg'] = $this->getFailedLoginMessage();
        return response()->json($msg);
    }

    /**
     * Verifica se o login foi efetuado com sucesso
     *
     * @param Request $request
     * @param $throttles
     * @return \Illuminate\Http\JsonResponse
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        // Verificações em relação ao login que foi realizado com suceso
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }

        $redirect = url($this->redirectPath);

        // Retorna mensagem de sucesso "logado com sucesso"
        $msg['type'] = 'success';
        $msg['msg'] = 'Logado com Sucesso!';
        $msg['redirect'] = $redirect;
        return response()->json($msg);
    }

    /**
     * Realiza o logout
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLogout()
    {
        // Realiza o logout
        Auth::logout();

        // Retorna um redirect
        return redirect($this->redirectAfterLogout);
    }

    /**
     * Retorna as credenciais
     *
     * @param Request $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        // Retorna as credenciais
        return $request->only($this->loginUsername(), 'password');
    }

    /**
     * Retorna mensagem se o login não foi efetuado
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        // Caso retorne falha no login
        return Lang::has('auth.failed')
            ? Lang::get('auth.failed')
            : 'These credentials do not match our records.';
    }

    /**
     * Retorna o LoginPatch se o login não foi efetuado
     *
     * @return string
     */
    public function loginPath()
    {
        // Recebe o loginPatch caso retorne falha ao Logar
        return property_exists($this, 'loginPath') ? $this->loginPath : '/login';
    }

    /**
     * Retorna o e-mail ou usuário digitado
     *
     * @return string
     */
    public function loginUsername()
    {
        // Retorna o usuário ou o email
        return property_exists($this, 'username') ? $this->username : 'email';
    }

    /**
     * Verifica a trait ThrottlesLoginsTrait
     *
     * @return bool
     */
    protected function isUsingThrottlesLoginsTrait()
    {
        // Verifica se está usando a trait throttlesLogins
        return in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );
    }

    /**
     * Redirecionar o usuário depois de determinar que seja bloqueado
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendLockoutResponse(Request $request)
    {
        // Captura o tempo sem segundos
        $seconds = app(RateLimiter::class)->availableIn(
            $this->getThrottleKey($request)
        );

        // Retorna mensagem de erro "bloqueado o login"
        $msg['type'] = 'error';
        $msg['msg'] = $this->getLockoutErrorMessage($seconds);
        return response()->json($msg);
    }
}
