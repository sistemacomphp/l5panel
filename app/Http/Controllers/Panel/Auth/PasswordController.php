<?php

namespace App\Http\Controllers\Painel\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

/**
 * PasswordController - Painel
 *
 * @author João Gustavo Balestrin dos Santos <joaogustavo.b@hotmail.com>
 * @copyright (c) 2016
 * @package LaravelPanel
 */
class PasswordController extends Controller
{
    /**
     * Assunto e-mail de redefinição
     *
     * @var
     */
    protected $subject;

    /**
     * Local a ser redirecionado após a redefinição da senha
     *
     * @var string
     */
    protected $redirectPath = '/login';

    /**
     * PasswordController construtor.
     */
    public function __construct()
    {
        // Middleware Guest
        $this->middleware('guest');
    }

    /**
     * Realiza o envio do e-mail de redefinição senha
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEmail(Request $request)
    {
        // Valida os dados
        $this->validate($request, ['email' => 'required|email']);

        // Assunto do e-mail
        $this->subject = 'Redefinir Senha';

        // Envio do e-mail
        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        // Verifica o retorno
        switch ($response) {
            // Caso foi enviado o e-mail com sucesso
            case Password::RESET_LINK_SENT:
                $msg['type'] = 'success';
                $msg['msg'] = trans($response);
                return response()->json($msg);

            // Caso tenha problemas com o usuário "e-mail"
            case Password::INVALID_USER:
                $msg['type'] = 'error';
                $msg['msg'] = trans($response);
                return response()->json($msg);
        }
    }

    /**
     * Reseta para a nova senha
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postReset(Request $request)
    {
        // Valida os dados
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        // Realiza a redefinição da senha
        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        // Verifica o retorno
        switch ($response) {
            // Caso a senha for redefinida com sucesso
            case Password::PASSWORD_RESET:
                $msg['type'] = 'success';
                $msg['msg'] = 'Senha Redefinida com Sucesso!';
                $msg['redirect'] = url($this->redirectPath);
                return response()->json($msg);

            // Caso tenha problemas
            default:
                $msg['type'] = 'error';
                $msg['msg'] = trans($response);
                return response()->json($msg);
        }
    }

    /**
     * Retorna o assunto do e-mail
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : 'Your Password Reset Link';
    }

    /**
     * Retorna a view reset, se existir o token
     *
     * @param null $token
     * @return $this
     */
    public function getReset($token = null)
    {
        if (is_null($token)) {
            abort(404);
        }

        return view('panel.auth.reset')->with('token', $token);
    }

    /**
     * Realiza a alteração da senha
     *
     * @param $user
     * @param $password
     */
    protected function resetPassword($user, $password)
    {
        $user->password = $password;

        $user->save();
    }

    /**
     * Retorna o diretório de redirecionamento apos a redefinição da senha
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }
}
