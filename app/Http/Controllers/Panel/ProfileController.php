<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PanelProfileRequest;
use App\Repositories\Contracts\UserRepositoryInterface;

/**
 * ProfileController - Painel
 *
 * @author João Gustavo Balestrin dos Santos <joaogustavo.b@hotmail.com>
 * @copyright (c) 2016
 * @package LaravelPanel
 */
class ProfileController extends Controller
{
    /**
     * Instancia do UserRepository
     *
     * @var UserRepositoryInterface
     */
    protected $repository;

    /**
     * ProfileController construtor.
     *
     * @param UserRepositoryInterface $repository
     */
    function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Exibe o formulário de editar perfil
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        // Busca o usuário logado
        $user = auth()->user();

        // Retorna a view de editar
        return view('panel.profile.edit', compact('user'));
    }

    /**
     * Atualiza o usuário na base de dados
     *
     * @param PanelProfileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PanelProfileRequest $request)
    {
        // Busca o usuário logado
        $user = auth()->user();

        // Realizar o update no repository
        $update = $this->repository->update($user, array_except($request->all(), ['role_id']));

        // Verifica se houve alguma falha no update
        if (!$update) {
            // Retorna uma mensagem de erro
            return response()->json(['type' => 'error', 'msg' => 'Falha na edição do usuário', 'redirect' => route('panel.profile')]);
        }

        // Retorna uma mensagem de sucesso
        return response()->json(['type' => 'success', 'msg'  => 'Atualizado com Sucesso!', 'redirect' => route('panel.profile')]);
    }
}
