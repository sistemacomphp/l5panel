<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class PanelUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'name' => 'min:2|max:255|required',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'min:6|required|confirmed',
                    'status' => 'required|in:0,1',
                    'role_id' => 'required|exists:roles,id'
                ];
            }

            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name' => 'min:2|max:255|required',
                    'email' => 'required|email|max:255|unique:users,email,'.$this->users,
                    'status' => 'required|in:0,1',
                    'password' => 'min:6|confirmed',
                    'role_id' => 'required|exists:roles,id'
                ];
            }
        }
    }
}
