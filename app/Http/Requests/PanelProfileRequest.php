<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PanelProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = auth()->user();

        return [
            'name' => 'min:2|max:255|required',
            'email' => 'required|email|max:255|unique:users,email,'.$user->id,
            'password' => 'min:6|confirmed',
            'status' => 'required|in:0,1'
        ];
    }
}
