<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// AuthController do Painel
Route::get('login', ['as' => 'panel.login', 'uses' => 'Panel\Auth\AuthController@getLogin']);
Route::post('login', ['as' => 'panel.login', 'uses' => 'Panel\Auth\AuthController@postLogin']);
Route::get('logout', ['as' => 'panel.logout', 'uses' => 'Panel\Auth\AuthController@getLogout']);

// PasswordController do Painel
Route::post('password/email', ['as' => 'panel.password.email', 'uses' => 'Panel\Auth\PasswordController@postEmail']);
Route::get('password/reset/{token}', ['as' => 'panel.reset.token', 'uses' => 'Panel\Auth\PasswordController@getReset']);
Route::post('password/reset', ['as' => 'panel.reset', 'uses' => 'Panel\Auth\PasswordController@postReset']);

// Grupo de Rotas do Painel
Route::group(['middleware' => ['auth'], 'prefix' => '/'], function() {
    // Dashboard
    Route::get('/', ['as' => 'panel', 'uses' => 'Panel\DashboardController@index']);

    // My Profile
    Route::get('profile', ['as' => 'panel.profile', 'uses' => 'Panel\ProfileController@edit']);
    Route::put('profile', ['as' => 'panel.profile', 'uses' => 'Panel\ProfileController@update']);

    // Admin
    Route::group(['middleware' => ['roles'], 'roles' => ['admin'], 'as' => 'panel.admin.'], function() {
        // Users
        Route::resource('users', 'Panel\Admin\UserController', ['except' => ['destroy']]);
    });
});