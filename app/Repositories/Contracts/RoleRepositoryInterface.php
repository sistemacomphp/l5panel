<?php

namespace App\Repositories\Contracts;

use Artesaos\Warehouse\Contracts\BaseRepository;

/**
 * Interface RoleRepositoryInterface
 *
 * @author João Gustavo Balestrin dos Santos <joaogustavo.b@hotmail.com>
 * @copyright (c) 2016
 * @package LaravelPanel
 */
interface RoleRepositoryInterface extends BaseRepository
{
    //
}