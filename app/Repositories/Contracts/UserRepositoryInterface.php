<?php

namespace App\Repositories\Contracts;

use Artesaos\Warehouse\Contracts\BaseRepository;
use Artesaos\Warehouse\Contracts\Segregated\CrudRepository;

/**
 * Interface UserRepositoryInterface
 *
 * @author João Gustavo Balestrin dos Santos <joaogustavo.b@hotmail.com>
 * @copyright (c) 2016
 * @package LaravelPanel
 */
interface UserRepositoryInterface extends CrudRepository, BaseRepository
{
    /**
     * Retorna os usuários mais recentes
     *
     * @param int $take
     * @param bool $paginate
     * @return mixed
     */
    public function getAllRecent($take = 15, $paginate = false);

    /**
     * Retorna o total de usuários ativos
     *
     * @return mixed
     */
    public function countAllActive();
}