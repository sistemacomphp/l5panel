<?php

namespace App\Repositories;

use App\Role;
use Artesaos\Warehouse\AbstractCrudRepository;
use App\Repositories\Contracts\RoleRepositoryInterface;

/**
 * RoleRepository
 *
 * @author João Gustavo Balestrin dos Santos <joaogustavo.b@hotmail.com>
 * @copyright (c) 2016
 * @package LaravelPanel
 */
class RoleRepository extends AbstractCrudRepository implements RoleRepositoryInterface
{
    /**
     * @var Role $modelClass
     */
    protected $modelClass = Role::class;
}