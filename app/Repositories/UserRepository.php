<?php

namespace App\Repositories;

use App\User;
use Artesaos\Warehouse\AbstractCrudRepository;
use App\Repositories\Contracts\UserRepositoryInterface;

/**
 * UserRepository
 *
 * @author João Gustavo Balestrin dos Santos <joaogustavo.b@hotmail.com>
 * @copyright (c) 2016
 * @package LaravelPanel
 */
class UserRepository extends AbstractCrudRepository implements UserRepositoryInterface
{
    /**
     * @var User $modelClass
     */
    protected $modelClass = User::class;

    /**
     * Retorna os usuários mais recentes
     *
     * @param int $take
     * @param bool|true $paginate
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Pagination\AbstractPaginator
     */
    public function getAllRecent($take = 15, $paginate = false)
    {
        $query = $this->newQuery();
        $query->orderBy('id', 'DESC');

        return $this->doQuery($query, $take, $paginate);
    }

    /**
     * Retorna o total de usuários ativos
     *
     * @return int
     */
    public function countAllActive()
    {
        $query = $this->newQuery();
        $query->where('status', 1);

        return $query->count();
    }

    /**
     * Salva o usuário na base de dados
     *
     * @param array $data
     * @return bool
     */
    public function create(array $data = [])
    {
        $model = $this->factory($data);
        $password = array_get($data, 'password', null);
        $role_id = array_get($data, 'role_id', null);

        if (!empty($password)) {
            $model->password = $data['password'];
        }

        if (!empty($role_id)) {
            $model->role_id = $data['role_id'];
        }

        $this->save($model);

        return $model;
    }

    /**
     * Atualiza o usuário na base de dados
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param array $data
     * @return bool
     */
    public function update($model, array $data = [])
    {
        $this->setModelData($model, $data);

        if (isset($data['password'])) {
            if ($data['password']) {
                $model->password = $data['password'];
            }
        }

        if (isset($data['role_id'])) {
            if ($data['role_id']) {
                $model->role_id = $data['role_id'];
            }
        }

        return $this->save($model);
    }
}