<?php

if (! function_exists('gravatar_image')) {
    /**
     * Retorna a URL do Gravatar através do e-mail
     *
     * @param $email
     * @return string
     */
    function gravatar_image($email)
    {
        $avatar = md5(strtolower(trim($email)));
        $urlencode = urlencode('http://i.imgur.com/05bBmuX.jpg');
        return "http://www.gravatar.com/avatar/{$avatar}?d={$urlencode}";
    }
}