<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * User - Eloquent Model
 *
 * @author João Gustavo Balestrin dos Santos <joaogustavo.b@hotmail.com>
 * @copyright (c) 2016
 * @package LaravelPanel
 */
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * Tabela do banco de dados utilizada pela model
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Atributos guardados (protegidos)
     *
     * @var array
     */
    protected $guarded = ['role_id'];

    /**
     * Atributos assinaláveis em massa
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'status'];

    /**
     * Colunas que devem convertidas para um tipo
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * Atributo das colunas de data
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Os atributos excluídos no retorno do model
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Converte a senha para Bcrypt
     *
     * @param $value
     * @return string
     */
    public function setPasswordAttribute($value)
    {
        // Converte o campo password para Bcrypt
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * Retorna o grupo que o usuário está relacionado
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function role()
    {
        return $this->hasOne('App\Role', 'id', 'role_id');
    }

    /**
     * Verifica se o usuário pertece ao grupo
     *
     * @param $roles
     * @return bool
     */
    public function hasRole($roles)
    {
        $have_role = $this->getUserRole();

        // Check if the user is a root account
        /*
        if ($have_role->name == 'admin') {
            return true;
        }
        */

        if (is_array($roles)) {
            foreach ($roles as $need_role) {
                if ($this->checkIfUserHasRole($need_role)) {
                    return true;
                }
            }
        } else {
            return $this->checkIfUserHasRole($roles);
        }

        return false;
    }

    /**
     * @return mixed
     */
    private function getUserRole()
    {
        return $this->role()->getResults();
    }

    /**
     * @param $need_role
     * @return bool
     */
    private function checkIfUserHasRole($need_role)
    {
        $have_role = $this->getUserRole();

        return (strtolower($need_role)==strtolower($have_role->name)) ? true : false;
    }
}
