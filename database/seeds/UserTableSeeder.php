<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        User::create([
            'role_id' => 1,
            'name' => 'João Gustavo',
            'email' => 'joaogustavo.b@hotmail.com',
            'password' => '123456',
            'status' => 1
        ]);

        $this->command->info('User joaogustavo.b@hotmail.com created!');
    }
}
