/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// Desabilita as notificações no desktop
process.env.DISABLE_NOTIFIER = true;

var elixir = require('laravel-elixir');
var del = require('del');
var gulp = require('gulp');
var imagemin = require('gulp-imagemin');

elixir.extend('remove', function(path) {
    new elixir.Task('remove', function() {
        del(path);
    });
});

elixir(function(mix) {

    // Path Destination
    var base_dest = 'public/assets';

    // Cleanup Destination
    mix.remove([ base_dest + '/css', base_dest + '/js', base_dest + '/images', base_dest + '/fonts' ]);

    // Compile Styles
    mix.styles([
        "bootstrap.min.css",
        "normalize.min.css",
        "font-awesome.min.css",
        "toastr.min.css",
        "datatables.min.css",
        "style.css",
    ], base_dest + "/css/app.css");

    // Compile Scripts
    mix.scripts([
    	"jquery.min.js",
        "bootstrap.min.js",
        "toastr.min.js",
        "datatables.min.js",
        "app.js"
    ], base_dest + "/js/app.js");

    // Minify Images
    new elixir.Task('imagemin', function () {
        return gulp.src('resources/assets/images/*')
            .pipe(imagemin({
                optimizationLevel: 10,
                progressive: true,
                interlaced: true
            }))
            .on('error', function(e) {
                new elixir.Notification().error(e, 'ImageMin Failed!');
                this.emit('end');
            })
            .pipe(gulp.dest(base_dest + '/images'))
            .pipe(new elixir.Notification('ImageMin Complete!'))
    });

    // Copy Static Assets
    mix.copy('resources/assets/fonts', base_dest + '/fonts');

});