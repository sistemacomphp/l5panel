$(function(){
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    $('.ajax-post').on('submit', function(e) {

        // Bloqueia qualquer acao junto
        e.preventDefault();

        // Captura o elemento form
        var form = $(this);

        // Captura os dados da requisicao
        var action = $(form).attr('action');
        var post = $(form).serialize();

        // Desabilita o botao
        $(form).find('[name="enviarbtn"]').prop('disabled', true);

        // Remove as ultimas mensagens do toastr e dos campos
        if ( $('.has-error').length ){ $('.has-error').each(function() { $(this).removeClass('has-error'); }) };
        if ( $('.validation').length ){ $('.validation').each(function() { $(this).empty().remove(); }) };
        toastr.remove();

        // Insere o loader
        $(form).find('.btns').append("<i class='fa fa-spinner fa-spin fa-2x'></i>");

        // Realiza a requisicao ajax
        $.ajax({
            url: action,
            type: "POST",
            dataType: "json",
            data: post,
            success:function(data) {
                // Remove o loader
                $(form).find('.btns .fa-spin').remove();

                // Habilita o botao
                $(form).find('[name="enviarbtn"]').prop('disabled', false);

                // Verifica se existe o type/msg
                if (typeof data.type == 'undefined' || typeof data.msg == 'undefined') {

                    toastr.error('Ocorreu um erro, tente novamente!', 'Ops!');

                    setTimeout(function(){ location.reload(true); }, 1200);

                } else {

                    // Verefica o tipo de mensagem a mostrar
                    if (data.type == 'success') {

                        // Mostra a mensagem de sucesso
                        toastr.success(data.msg, 'Alerta!');

                    } else {

                        // Mostra a mensagem de erro
                        toastr.error(data.msg, 'Ops!');

                    }

                    // Verefica se tem redirect
                    if (data.redirect) {
                        setTimeout(function(){ $(location).attr('href', data.redirect); }, 1100);
                    } else {
                        if (data.type == 'success') {
                            // Limpa os campos
                            $(form).each(function(){
                                this.reset();
                            });
                        }
                    }
                }
            },
            error:function(data) {
                // Verefica se o status for 422 (Validacao inputs)
                if (data.status == 422) {

                    // Remove o loader
                    $(form).find('.btns .fa-spin').remove();

                    // Habilita o botao
                    $(form).find('[name="enviarbtn"]').prop('disabled', false);

                    // Transforma em JSON o retorno
                    var errors = $.parseJSON(data.responseText);

                    // Verefica o erro e retorna para o usuario
                    $.each(errors, function(key, value) {
                        parent = $(form).find('[name="' + key + '"]');

                        if (parent.length > 0) {

                            // Captura o elemento (Input)
                            var element_input = $(form).find('[name="' + key + '"]:first,[name="' + key + '[]"]:first');

                            // Verifica se existe a classe radio-list
                            if (element_input.parents('.radio-list').length > 0) {

                                // Insere a mensagem de erro apos a classe radio-list
                                element_input.parents('.radio-list').after("<label class='validation'>" + value[0] + "</label>");

                            } else {

                                // Insere a mensagem de erro apos o input
                                element_input.after("<label class='validation'>" + value[0] + "</label>");

                            }

                            // Insere classe de erro no form-group
                            element_input.parents('.form-group').addClass('has-error');

                        } else {
                            toastr.error(value, 'Ops!');
                        }
                    });

                } else {

                    // Caso contrario pode ser um erro do servidor (500, 404...

                    // Remove o loader
                    $(form).find('.btns .fa-spin').remove();

                    // Habilita o botao
                    $(form).find('[name="enviarbtn"]').prop('disabled', false);

                    // Retorna uma mensagem de notificacao
                    toastr.error('Problema na Requisição, tente novamente!', 'Ops!');

                    setTimeout(function(){ location.reload(true); }, 1200);

                }
            }
        });
    });

    $('.ajax-put').on('submit', function(e) {

        // Bloqueia qualquer acao junto
        e.preventDefault();

        // Captura o elemento form
        var form = $(this);

        // Captura os dados da requisicao
        var action = $(form).attr('action');
        var post = $(form).serialize();

        // Desabilita o botao
        $(form).find('[name="enviarbtn"]').prop('disabled', true);

        // Remove as ultimas mensagens do toastr e dos campos
        if ( $('.has-error').length ){ $('.has-error').each(function() { $(this).removeClass('has-error'); }) };
        if ( $('.validation').length ){ $('.validation').each(function() { $(this).empty().remove(); }) };
        toastr.remove();

        // Insere o loader
        $(form).find('.btns').append("<i class='fa fa-spinner fa-spin fa-2x'></i>");

        // Realiza a requisicao ajax
        $.ajax({
            url: action,
            type: "PUT",
            dataType: "json",
            data: post,
            success:function(data) {
                // Remove o loader
                $(form).find('.btns .fa-spin').remove();

                // Habilita o botao
                $(form).find('[name="enviarbtn"]').prop('disabled', false);

                // Verifica se existe o type/msg
                if (typeof data.type == 'undefined' || typeof data.msg == 'undefined') {

                    toastr.error('Ocorreu um erro, tente novamente!', 'Ops!');

                    setTimeout(function(){ location.reload(true); }, 1200);

                } else {

                    // Verefica o tipo de mensagem a mostrar
                    if (data.type == 'success') {

                        // Mostra a mensagem de sucesso
                        toastr.success(data.msg, 'Alerta!');

                    } else {

                        // Mostra a mensagem de erro
                        toastr.error(data.msg, 'Ops!');

                    }

                    // Verefica se tem redirect
                    if (data.redirect) {
                        setTimeout(function(){ $(location).attr('href', data.redirect); }, 1100);
                    } else {
                        if (data.type == 'success') {
                            // Limpa os campos
                            $(form).each(function(){
                                this.reset();
                            });
                        }
                    }
                }
            },
            error:function(data) {
                // Verefica se o status for 422 (Validacao inputs)
                if (data.status == 422) {

                    // Remove o loader
                    $(form).find('.btns .fa-spin').remove();

                    // Habilita o botao
                    $(form).find('[name="enviarbtn"]').prop('disabled', false);

                    // Transforma em JSON o retorno
                    var errors = $.parseJSON(data.responseText);

                    // Verefica o erro e retorna para o usuario
                    $.each(errors, function(key, value) {
                        parent = $(form).find('[name="' + key + '"]');

                        if (parent.length > 0) {

                            // Captura o elemento (Input)
                            var element_input = $(form).find('[name="' + key + '"]:first,[name="' + key + '[]"]:first');

                            // Verifica se existe a classe radio-list
                            if (element_input.parents('.radio-list').length > 0) {

                                // Insere a mensagem de erro apos a classe radio-list
                                element_input.parents('.radio-list').after("<label class='validation'>" + value[0] + "</label>");

                            } else {

                                // Insere a mensagem de erro apos o input
                                element_input.after("<label class='validation'>" + value[0] + "</label>");

                            }

                            // Insere classe de erro no form-group
                            element_input.parents('.form-group').addClass('has-error');

                        } else {
                            toastr.error(value, 'Ops!');
                        }
                    });

                } else {

                    // Caso contrario pode ser um erro do servidor (500, 404...)

                    // Remove o loader
                    $(form).find('.btns .fa-spin').remove();

                    // Habilita o botao
                    $(form).find('[name="enviarbtn"]').prop('disabled', false);

                    // Retorna uma mensagem de notificacao
                    toastr.error('Problema na Requisição, tente novamente!', 'Ops!');

                    setTimeout(function(){ location.reload(true); }, 1200);
                }
            }
        });
    });

    $("#forget-password").click(function() {
        $(".login-box").hide();
        $(".forget-box").show();
    });

    $("#back-btn").click(function() {
        $(".login-box").show();
        $(".forget-box").hide();
    });
    
    $('[data-toggle="tooltip"]').tooltip();

    $('#dataTable').DataTable({
        "aaSorting": [],
        responsive: true,
        "oLanguage": {
            "sEmptyTable": "Nenhum Item Encontrado!",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ itens",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 itens",
            "sInfoFiltered": "(Filtrados de _MAX_ Itens)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ &nbsp;itens por página",
            "sLoadingRecords": "Atualizando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum item encontrado!",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        }
    });
});