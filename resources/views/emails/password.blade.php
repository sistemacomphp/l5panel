<!DOCTYPE html>  
<html lang="pt-br">  
<head>  
	<meta charset="utf-8">  
</head>  
<body>
	<p>Olá! Foi solicitado uma redefinição de senha. Clique no link abaixo para redefinir sua senha:</p>
	<p><a href="{{ URL::to('password/reset', array($token)) }}" title="Recuperar Senha">{{ URL::to('password/reset', array($token)) }}</a></p>
	<p>O link tem um prazo de 24 horas de utilização, após esse prazo ele irá expirar.</p>
	<p>LaravelPanel - Sistema</p>
</body>
</html>