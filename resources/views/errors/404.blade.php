<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <title>Página Não Encontrada! | Erro 404 - LaravelPanel</title>
    
        <!-- Font Google -->
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">
    
        <!-- Viewport -->
        <meta content="width=device-width, initial-scale=1" name="viewport" />
    
        <!-- Style CSS -->
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #ada9a9;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 76px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Página não encontrada!</div>
            </div>
        </div>
    </body>
</html>
