@extends('panel/app')

@section('title', 'Cadastrar Usuário | LaravelPanel')

@section('content')
    <div class="container top_80">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel_mb panel-default">
                    <div class="panel-heading">Cadastrar Usuário</div>

                    <div class="panel-body">
                        <form action="{{ route('panel.admin.users.store') }}" class="form-horizontal ajax-post">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Nome</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" id="name" required />
                                </div>
                            </div>

                            <div class="form-group mgt_10">
                                <label class="col-md-4 control-label" for="email">E-mail</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" id="email" required />
                                </div>
                            </div>

                            <div class="form-group mgt_10">
                                <label class="col-md-4 control-label">Status</label>
                                <div class="col-md-6">
                                    <div class="radio-list">
                                        <label class="radio-inline"><input type="radio" name="status" value="1" checked>ativo</label>
                                        <label class="radio-inline"><input type="radio" name="status" value="0">inativo</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Grupo</label>
                                <div class="col-md-6">
                                    <div class="radio-list">
                                        @foreach($roles as $role)
                                            <label class="radio-inline"><input type="radio" name="role_id" value="{{ $role->id }}" required>{{ $role->display_name }}</label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password">Senha</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password" id="password" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password_confirmation">Confirmar Senha</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4 btns">
                                    <button type="submit" class="btn btn-success" name="enviarbtn">Cadastrar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection