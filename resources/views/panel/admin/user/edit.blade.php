@extends('panel/app')

@section('title', 'Editar Usuário | LaravelPanel')

@section('content')
    <div class="container top_80">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel_mb panel-default">
                    <div class="panel-heading">Editar Usuário</div>

                    <div class="panel-body">
                        <form action="{{ route('panel.admin.users.update', [$user->id]) }}" class="form-horizontal ajax-put">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Nome</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}" required />
                                </div>
                            </div>

                            <div class="form-group mgt_10">
                                <label class="col-md-4 control-label" for="email">E-mail</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" id="email" value="{{ $user->email }}" required>
                                </div>
                            </div>

                            <div class="form-group mgt_10">
                                <label class="col-md-4 control-label">Status</label>
                                <div class="col-md-6">
                                    <div class="radio-list">
                                        <label class="radio-inline"><input type="radio" name="status" value="1" {{ $user->status == 1 ? 'checked' : '' }}>ativo</label>
                                        <label class="radio-inline"><input type="radio" name="status" value="0" {{ $user->status == 0 ? 'checked' : '' }}>inativo</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Grupo</label>
                                <div class="col-md-6">
                                    <div class="radio-list">
                                        @foreach($roles as $role)
                                            <label class="radio-inline"><input type="radio" name="role_id" value="{{ $role->id }}" {{ $user->role->id == $role->id ? 'checked' : '' }}>{{ $role->display_name }}</label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            @if($user->role->name != 'admin' or $user->id == auth()->user()->id)
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="password">Nova Senha</label>
                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="password" id="password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="password_confirmation">Confirmar Senha</label>
                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
                                    </div>
                                </div>
                            @endif

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4 btns">
                                    <button type="submit" class="btn btn-success" name="enviarbtn">Atualizar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection