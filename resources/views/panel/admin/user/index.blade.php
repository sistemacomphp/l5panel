@extends('panel/app')

@section('title', 'Usuários | LaravelPanel')

@section('content')
    <div class="container top_80">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel_mb panel-default">
                    <div class="panel-heading">Usuários <div class="pull-right"><a href="{{ route('panel.admin.users.create') }}" class="btn btn-success">Cadastrar novo</a></div></div>

                    <div class="panel-body">
                        <table id="dataTable" class="table table-responsive table-striped table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Status</th>
                                    <th>Grupo</th>
                                    <th>Cadastrado em</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td class="pt14"><a href="{{ route('panel.admin.users.show', [$user->id]) }}" title="Mais Detalhes">{{ $user->name }}</a></td>
                                        <td class="pt14">{{ $user->email }}</td>
                                        <td class="pt14">
                                            @if($user->status == 1)
                                                <span class="label label-sm label-success">ativo</span>
                                            @else
                                                <span class="label label-sm label-danger">inativo</span>
                                            @endif
                                        </td>
                                        <td class="pt14">{{ $user->role->display_name }}</td>
                                        <td class="pt14">{{ $user->created_at->format('d/m/Y H:i:s') }}</td>
                                        <td>
                                            <a href="{{ route('panel.admin.users.edit', [$user->id]) }}" class="btn btn-sm btn-icon btn-flat btn-default inline-block font15" data-toggle="tooltip" data-original-title="Editar">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection