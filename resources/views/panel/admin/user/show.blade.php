@extends('panel/app')

@section('title', 'Detalhes do Usuário | LaravelPanel')

@section('content')
    <div class="container top_80">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel_mb panel-default">
                    <div class="panel-heading">Detalhes do Usuário</div>

                    <div class="panel-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">ID:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $user->id }} </p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nome:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $user->name }} </p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">E-mail:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $user->email }} </p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Status: </label>
                                        <div class="col-md-9">
                                            <p class="form-control-static">
                                                @if($user->status == 1)
                                                    <span class="label label-sm label-success"> ativo </span>
                                                @else
                                                    <span class="label label-sm label-danger"> inativo </span>
                                                @endif
                                            </p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Grupo:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $user->role->display_name }} <span class="blue-tooltip" data-toggle="tooltip" data-placement="right" title="" data-original-title="{{ $user->role->description }}" aria-describedby="tooltip155941"><i class="fa fa-question-circle-o" aria-hidden="true"></i></span></p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Cadastrado em:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $user->created_at->format('d/m/Y H:i:s') }} </p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Atualizado em:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $user->updated_at->format('d/m/Y H:i:s') }} </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection