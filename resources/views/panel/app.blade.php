@include('panel/header')

@include('panel/menu')

@yield('content')

@include('panel/footer_content')

@include('panel/footer')