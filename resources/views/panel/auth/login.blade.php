@extends('panel/auth/app')

@section('title', 'Faça seu Login | LaravelPanel')

@section('assets_header')
<style style="text/css">
    body { background: url('{{ asset('assets/images/bg-login.jpg') }}') no-repeat center center fixed; background-size: cover; padding: 0; margin: 0; }

    html { height:100%; }

    .btns .fa { margin: 22px 0 12px !important; position:static !important;}

    .form-group { margin: 0; }

    .fa-2x { font-size: 2.6em !important; }

    .validation {margin:0 0 20px 0 !important; color: red; }
</style>
@endsection

@section('content')
    <div class="login-page">
        <div class="form-login">
            <div class="login-box">            
                <h1 class="text-center login-title">Faça seu Login</h1>
                <form class="ajax-post" action="{{ route('panel.login') }}" method="post">
                    <div class="form-group">
                        <input type="text" name="email" placeholder="e-mail" required/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" placeholder="senha" required/>
                    </div>
                    <div class="btns"><button type="submit" name="enviarbtn">login</button></div>
                    <p class="message">Esqueceu a senha? <a class="cpointer" id="forget-password">Recuperar Senha</a></p>
                </form>
            </div>

            <div class="forget-box">
                <h1 class="text-center login-title">Recuperar Senha</h1>
                <form class="ajax-post" action="{{ route('panel.password.email') }}" method="post">
                    <div class="form-group">
                        <input type="text" name="email" placeholder="e-mail" required/>
                    </div>
                    <div class="btns"><button type="submit" name="enviarbtn">enviar</button></div>
                    <p class="message"><a class="cpointer" id="back-btn">Voltar ao Login</a></p>
                </form>
            </div>
        </div>
    </div>
@endsection