@extends('panel/auth/app')

@section('title', 'Redefina sua Senha | LaravelPanel')

@section('assets_header')
    <style style="text/css">
        body { background: url('{{ asset('assets/images/bg-login.jpg') }}') no-repeat center center fixed; background-size: cover; padding: 0; margin: 0; }

        html { height:100%; }

        .btns .fa { margin: 22px 0 12px; }

        .form-group { margin: 0; }

        .fa-2x { font-size: 2.6em !important; }

        .forget-text { margin-top:-1px; margin-bottom:24px; }

        .validation {margin-bottom:20px; color: red; }
    </style>
@endsection

@section('content')
    <div class="login-page">
        <div class="form-login">
            <h1 class="text-center forget-title">Redefina sua Senha</h1>
            <p class="forget-text"> Para redefinir sua senha, digite abaixo seu e-mail e uma nova senha:</p>
            <form class="ajax-post" action="{{ route('panel.reset') }}" method="post">
                <div class="form-group">
                    <input type="text" name="email" placeholder="e-mail" required/>
                </div>

                <div class="form-group">
                    <input type="password" name="password" placeholder="senha" required/>
                </div>

                <div class="form-group">
                    <input type="password" name="password_confirmation" placeholder="confirmar nova senha" required/>
                </div>

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="btns"><button type="submit" name="enviarbtn">Redefinir</button></div>
            </form>
        </div>
    </div>
@endsection