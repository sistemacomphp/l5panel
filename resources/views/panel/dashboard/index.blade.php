@extends('panel/app')

@section('title', 'Dashboard | LaravelPanel')

@section('content')
    <div class="container top_80">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel_mb panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <p>Você está logado!</p>
                        <!--<h3>Meus dados:</h3>
                        <ul>
                            <li>Nome: {{-- auth()->user()->name --}}</li>
                            <li>E-mail: {{-- auth()->user()->email --}}</li>
                            <li>Status: {{-- (auth()->user()->status == 1) ? 'Ativo' : 'Inativo' --}}</li>
                            <li>Cadastrado em: {{-- auth()->user()->created_at->format('d/m/Y H:i:s') --}}</li>
                            <br />
                            <li>Nome do Grupo: {{-- auth()->user()->role->display_name --}}</li>
                            <li>Descrição do Grupo: {{-- auth()->user()->role->description --}}</li>
                        </ul>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection