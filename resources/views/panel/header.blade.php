<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">
    
    <!-- Viewport -->
    <meta content="width=device-width, initial-scale=1" name="viewport" />

    <!-- Painel Styles -->
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" type="text/css" />

    @yield('assets_header')
</head>
<body>