<!-- Navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('panel') }}">LaravelPanel</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ (request()->is('/') ? 'active' : '') }}"><a href="{{ route('panel') }}">Dashboard</a></li>

                @if(auth()->user()->role->name == 'admin')
                    <li class="{{ (request()->is('users') ? 'active' : '') }}"><a href="{{ route('panel.admin.users.index') }}">Usuários</a></li>
                @endif
            </ul>

            <ul class="nav navbar-nav navbar-right navbar-nav-avatar">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle no-pd-nav" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ auth()->user()->name }}
                        <img alt="" class="img-circle avatar-menu" src="{{ gravatar_image(auth()->user()->email) }}">
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-painel" role="menu">
                        <li class="{{ (request()->is('profile') ? 'active' : '') }}"><a href="{{ route('panel.profile') }}">Meu Perfil</a></li>
                        <li><a href="{{ route('panel.logout') }}">Sair</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>