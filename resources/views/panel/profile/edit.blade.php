@extends('panel/app')

@section('title', 'Meu Perfil | LaravelPanel')

@section('content')
    <div class="container top_80">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel_mb panel-default">
                    <div class="panel-heading">Meu Perfil</div>

                    <div class="panel-body">
                        <form action="{{ route('panel.profile') }}" class="form-horizontal ajax-put">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Nome</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}" required />
                                </div>
                            </div>

                            <div class="form-group mgt_10">
                                <label class="col-md-4 control-label" for="email">E-mail</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" id="email" value="{{ $user->email }}" required>
                                </div>
                            </div>

                            <div class="form-group mgt_10">
                                <label class="col-md-4 control-label">Status</label>
                                <div class="col-md-6">
                                    <div class="radio-list">
                                        <label class="radio-inline"><input type="radio" name="status" value="1" {{ $user->status == 1 ? 'checked' : '' }}>ativo</label>
                                        <label class="radio-inline"><input type="radio" name="status" value="0" {{ $user->status == 0 ? 'checked' : '' }}>inativo</label>
                                        <p class="help-block">Obs: Ao selecionar inativo, sua conta será desativada!</p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password">Nova Senha</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password_confirmation">Confirmar Senha</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4 btns">
                                    <button type="submit" class="btn btn-success" name="enviarbtn">Atualizar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection